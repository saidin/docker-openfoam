FROM ubuntu:14.04

RUN apt-get update
RUN apt-get install -y --no-install-recommends git-core 
RUN apt-get install -y --no-install-recommends binutils-dev
RUN apt-get install -y --no-install-recommends cmake flex
RUN apt-get install -y --no-install-recommends bison 
RUN apt-get install -y --no-install-recommends qt4-dev-tools 
RUN apt-get install -y --no-install-recommends libqt4-dev 
RUN apt-get install -y --no-install-recommends libqtwebkit-dev 
RUN apt-get install -y --no-install-recommends gnuplot
RUN apt-get install -y --no-install-recommends libreadline-dev 
RUN apt-get -y update
RUN apt-get install -y --no-install-recommends --fix-missing libncurses-dev 
RUN apt-get install -y --no-install-recommends libxt-dev 
RUN apt-get install -y --no-install-recommends libopenmpi-dev 
RUN apt-get install -y --no-install-recommends openmpi-bin 
RUN apt-get install -y --no-install-recommends libboost-system-dev 
RUN apt-get install -y --no-install-recommends libboost-thread-dev 
RUN apt-get install -y --no-install-recommends libgmp-dev 
RUN apt-get install -y --no-install-recommends libmpfr-dev 
RUN apt-get install -y --no-install-recommends python python-dev
RUN apt-get install -y --no-install-recommends libglu1-mesa-dev libqt4-opengl-dev
RUN apt-get install -y --no-install-recommends wget time 
RUN apt-get install -y --no-install-recommends libboost-system1.55.0 libboost-thread1.55.0 libboost1.55-dev
RUN apt-get install -y --no-install-recommends build-essential 
RUN apt-get install -y --no-install-recommends zlib1g-dev 

RUN cd ~ && \
    mkdir OpenFOAM && \
    cd OpenFOAM && \
    wget  --no-check-certificate "http://download.openfoam.org/source/4-1" -O OpenFOAM-4.1.tgz && \
    wget  --no-check-certificate "http://download.openfoam.org/third-party/4-1" -O ThirdParty-4.1.tgz && \    
    tar -xzf OpenFOAM-4.1.tgz && \
    tar -xzf ThirdParty-4.1.tgz && \    
    mv OpenFOAM-4.x-version-4.1 OpenFOAM-4.1 && \
    mv ThirdParty-4.x-version-4.1 ThirdParty-4.1 && \
    cd ThirdParty-4.1 && \
    mkdir download && \
    wget --no-check-certificate -P download http://www.cmake.org/files/v3.2/cmake-3.2.1.tar.gz && \
    wget --no-check-certificate -P download https://github.com/CGAL/cgal/releases/download/releases%2FCGAL-4.8/CGAL-4.8.tar.xz && \
    tar -xzf download/cmake-3.2.1.tar.gz && \
    tar -xJf download/CGAL-4.8.tar.xz 

RUN cd ~/OpenFOAM && \
    ln -s /usr/bin/mpicc.openmpi OpenFOAM-4.1/bin/mpicc && \
    ln -s /usr/bin/mpirun.openmpi OpenFOAM-4.1/bin/mpirun

RUN cd ~/OpenFOAM && \ 
    sed -i -e 's/\(cgal_version=\)cgal-system/\1CGAL-4.8/' OpenFOAM-4.1/etc/config.sh/CGAL 

RUN echo "export QT_SELECT=qt4">>$HOME/OpenFOAM/OpenFOAM-4.1/etc/bashrc
RUN echo "source \$HOME/OpenFOAM/OpenFOAM-4.1/etc/bashrc FOAMY_HEX_MESH=yes" >> ~/.bashrc

SHELL ["/bin/bash", "-c"]

RUN source /root/OpenFOAM/OpenFOAM-4.1/etc/bashrc FOAMY_HEX_MESH=yes && \
    cd $WM_THIRD_PARTY_DIR && \
    ./makeCmake && \
    export QT_SELECT=qt4 && \
    ./makeParaView -python -mpi -python-lib /usr/lib/x86_64-linux-gnu/libpython2.7.so.1.0

RUN ln -s /usr/lib/x86_64-linux-gnu/libboost_system.so.1.55.0 /usr/lib/x86_64-linux-gnu/libboost_system.so && \
    ln -s /usr/lib/x86_64-linux-gnu/libboost_thread.so.1.55.0  /usr/lib/x86_64-linux-gnu/libboost_thread.so

RUN source /root/OpenFOAM/OpenFOAM-4.1/etc/bashrc FOAMY_HEX_MESH=yes && \
    cd $WM_PROJECT_DIR && \
    export QT_SELECT=qt4 &&    ./Allwmake -j 2

RUN echo "export FOAM_RUN=/root/OpenFOAM/root-4.1/run">> $HOME/OpenFOAM/OpenFOAM-4.1/etc/bashrc

RUN source /root/OpenFOAM/OpenFOAM-4.1/etc/bashrc FOAMY_HEX_MESH=yes && \
    mkdir -p /root/OpenFOAM/root-4.1/run && \
    cd /root/OpenFOAM/root-4.1/run && ls && \
    git config --global http.sslVerify false && \
    git clone -v https://github.com/Unofficial-Extend-Project-Mirror/openfoam-extend-swak4Foam-dev.git swak4Foam  && \
    cd swak4Foam && \
    git checkout branches/develop

RUN source /root/OpenFOAM/OpenFOAM-4.1/etc/bashrc FOAMY_HEX_MESH=yes && \
    cd /root/OpenFOAM/root-4.1/run/swak4Foam && \
    ./maintainanceScripts/compileRequirements.sh && \
    ./Allwmake